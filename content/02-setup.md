---
title: "Setup"
date: 2018-09-07T14:35:16-05:00
weight: 100
---
# Setup

For this workshop we're using a vagrant image - it is a Windows Server 2016 image with docker pre-installed.
It is **strongly** advised that you set up this environment and test that it works prior to the workshop if _at all_ possible.
Once the lab environment is set up you should _not_ need to download anything else.

## Prerequisites:

You must have Vagrant and a hypervisor (Hyper-V or VirtualBox) installed.

- [instructions for installing Vagrant][instructions-vagrant]
- [Instructions for installing Hyper-V][instructions-hyperv]
  - [Instructions for adding an external switch][instructions-hyperv-switch].
    This is necessary so that your machine can connect to the internet to download some prerequisites.
- [Instructions for installing VirtualBox][instructions-virtualbox]

## Steps

1. Download the `Vagrantfile` from [this link][project-download] into a folder on your machine.
  - If it appended the `.txt` extension to the file, remove it.
    The file should just be `Vagrantfile`, no extension.
2. Open a terminal, navigate to the folder where you saved the `VagrantFile`, and run the command below:
  - `vagrant up --provision`
3. This will download and spin up the lab machine for this workshop and get you ready to learn some PowerShell!

## Controlling the Environment

You can _pause and resume_ the environment:

- `vagrant suspend`
- `vagrant resume`

You can _halt_ the environment, freeing up RAM:

- `vagrant halt`

You can _destroy_ the environment, freeing up disk space:

- `vagrant destroy`

You can bring the environment back up after _halting_ or _destroying_ it using the same command as when you first brought it up:

- `vagrant up --provision`

[project-download]:        https://gitlab.com/michaeltlombardi/pwshop/raw/master/Vagrantfile?inline=false
[instructions-hyperv]:     https://docs.microsoft.com/en-us/virtualization/hyper-v-on-windows/quick-start/enable-hyper-v
[instructions-virtualbox]: https://www.virtualbox.org/wiki/Downloads
[instructions-vagrant]:    https://www.vagrantup.com/intro/getting-started/install.html
[instructions-hyperv-switch]: https://docs.microsoft.com/en-us/windows-server/virtualization/hyper-v/get-started/create-a-virtual-switch-for-hyper-v-virtual-machines
